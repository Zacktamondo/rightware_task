import 'package:flutter/material.dart';
import 'package:rightware_app/res/colors.dart';
import 'package:rightware_app/widgets/homepage_widgets.dart';


class Homepage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Scaffold(
        backgroundColor: createMaterialColor(AppColors.mainScreenBackgroundColor),
        body: HomepageWidgets(),
      ),
    );
  }
}
