import 'dart:async';

import 'package:flutter/material.dart';
import 'package:rightware_app/widgets/splash_widgets.dart';

class SplashScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen>
    with SingleTickerProviderStateMixin {

   _moveToHome() {
    Navigator.of(context).pop();
    Navigator.pushNamedAndRemoveUntil(
        context, "/a", (_) => false); //to move to another page with no history
  }

  @override
  void initState() {
    super.initState();
    Timer(Duration(milliseconds: 2000), () => {_moveToHome()()});
  }

  @override
  Widget build(BuildContext context) {
    return SplashWidgets();
  }
}
