import 'package:flutter/material.dart';
import 'package:rightware_app/res/colors.dart';

class SplashWidgets extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: createMaterialColor(AppColors.mainScreenBackgroundColor),
      body: Stack(
        children: <Widget>[
          Align(
            alignment: Alignment.topCenter,
            child: Padding(
                padding: EdgeInsets.only(top: 35.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    FlutterLogo(size: 150),
                    Text(
                      'Rightware Delivery app',
                      style: TextStyle(
                        color: createMaterialColor(AppColors.colorPrimaryDark),
                        fontSize: 32.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 135.0),
                    ),
                    CircularProgressIndicator(),
                  ],
                )),
          ),
        ],
      ),
    );
  }
}
