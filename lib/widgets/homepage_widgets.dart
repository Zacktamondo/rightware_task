import 'package:flutter/material.dart';
import 'package:rightware_app/models/shop.dart';
import 'package:rightware_app/res/colors.dart';
import 'package:rightware_app/utils/api.dart';
import 'package:rightware_app/widgets/shop_widgets.dart';

class HomepageWidgets extends StatefulWidget {
  HomepageWidgets({Key? key}) : super(key: key);

  @override
  _HomepageWidgetsState createState() => _HomepageWidgetsState();
}

class _HomepageWidgetsState extends State<HomepageWidgets> {
  @override
  Widget build(BuildContext context) {
    API api = API();
    return Scaffold(
      appBar: AppBar(
        title: Text("Rightware Delivery"),
        actions: [
          IconButton(onPressed: () => {}, icon: Icon(Icons.shopping_cart))
        ],
      ),
      body: FutureBuilder(
        future: api.getShops(),
        builder: (context, AsyncSnapshot snapshot) {
          if (!snapshot.hasData) {
            return LoadingWidget();
          } else {
            return
                // Trying to add a search field
                // Row(
                // mainAxisAlignment: MainAxisAlignment.center,
                // children: [
                //Card(
                //     child: Form(
                //       child: Container(
                //         width: 300,
                //         height: 50,
                //         alignment: Alignment.center,
                //         decoration: BoxDecoration(
                //             border: Border.all(
                //                 width: 1,
                //                 color:
                //                     createMaterialColor(AppColors.colorPrimary)),
                //             borderRadius: BorderRadius.circular(20)),
                //         child: TextFormField(
                //           decoration: InputDecoration(
                //               labelText: "Search",
                //               suffixIcon: Icon(Icons.search),
                //               border: InputBorder.none,
                //               contentPadding: EdgeInsets.all(10)),
                //         ),
                //       ),
                //     ),
                //   ),
                // The Resturants data
                Card(
              child: GridView.builder(
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 1,
                    childAspectRatio: 1.3,
                    mainAxisSpacing: 1,
                  ),
                  itemCount: snapshot.data.length,
                  itemBuilder: (BuildContext context, index) {
                    Shop shop = snapshot.data[index];
                    return ShopWidgets(shop: shop);
                  }),
            );
            //   ],
            // );
          }
        },
      ),
    );
  }

// A widget to show while loading the data
  Widget LoadingWidget() {
    return Center(
        child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          "Loading data...",
          style: TextStyle(fontSize: 24.0),
        ),
        Padding(
            padding: EdgeInsets.all(15.0), child: CircularProgressIndicator())
      ],
    ));
  }
}
