import 'package:flutter/material.dart';
import 'package:rightware_app/models/shop.dart';
import 'package:rightware_app/res/colors.dart';

class ShopWidgets extends StatefulWidget {
  const ShopWidgets({
    Key? key,
    required this.shop,
  }) : super(key: key);

  final Shop shop;

  @override
  _ShopWidgetsState createState() => _ShopWidgetsState();
}

class _ShopWidgetsState extends State<ShopWidgets> {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Card(
      elevation: 8.0,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            margin: EdgeInsets.only(bottom: 10, right: 10, left: 10),
            width: size.width,
            height: size.height * 0.3,
            decoration: BoxDecoration(
              boxShadow: [
                BoxShadow(
                  color: Colors.grey,
                  blurRadius: 2,
                  offset: Offset(2, 2), // Shadow position
                ),
              ],
              borderRadius: BorderRadius.all(Radius.circular(20)),
              image: DecorationImage(
                  image: NetworkImage(widget.shop.coverPhoto.toString()),
                  fit: BoxFit.cover),
            ),
            child: DecoratedBox(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(25),
                ),
                child: Stack(
                  children: [
                    Positioned(
                      left: size.width * 0.03,
                      bottom: size.height * 0.035,
                      height: 20,
                      child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Expanded(
                              child: Text('${widget.shop.shopName!.en}',
                                  style: TextStyle(
                                      fontSize: 12.0,
                                      color: createMaterialColor(
                                          AppColors.colorAccent))),
                            )
                          ]),
                    ),
                    Positioned(
                      left: size.width * 0.03,
                      bottom: size.height * 0.017,
                      child: Text('${widget.shop.address!.city}',
                          style: TextStyle(
                              fontSize: 12.0,
                              color:
                                  createMaterialColor(AppColors.colorAccent))),
                    ),
                    Positioned(
                      right: size.width * 0.03,
                      bottom: size.height * 0.017,
                      child: Text(
                          '${widget.shop.minimumOrder!.amount}  ${widget.shop.minimumOrder!.currency}',
                          style: TextStyle(
                              fontSize: 10.0,
                              color:
                                  createMaterialColor(AppColors.colorAccent))),
                    ),
                    Positioned(
                      top: size.height * 0.005,
                      right: size.width * 0.03,
                      child: Container(
                        padding: EdgeInsets.all(2),
                        height: size.height * 0.02,
                        width: size.width * 0.2,
                        decoration: BoxDecoration(
                            color: createMaterialColor(AppColors.colorPrimary),
                            borderRadius: BorderRadius.circular(20)),
                        child: Center(
                          child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                Icon(
                                  Icons.access_time_outlined,
                                  size: 10,
                                  color: Colors.white,
                                ),
                                Text(
                                  '${widget.shop.estimatedDeliveryTime}',
                                  style: TextStyle(
                                      fontSize: 12.0,
                                      color: createMaterialColor(
                                          AppColors.colorAccent)),
                                ),
                              ]),
                        ),
                      ),
                    )
                  ],
                )),
          ),
          Container(
              child: Center(
                  child: Text('${widget.shop.description!.en}',
                      style: TextStyle(
                          fontSize: 12.0,
                          color: createMaterialColor(AppColors.textColor))))),
        ],
      ),
    );
    // );
  }
}
