import 'package:flutter/material.dart';
import 'package:rightware_app/pages/homepage_page.dart';
import 'package:rightware_app/pages/splash_page.dart';
import 'package:rightware_app/res/colors.dart';
void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
          primarySwatch: createMaterialColor(AppColors.colorPrimary),
          canvasColor:
              createMaterialColor(AppColors.mainScreenBackgroundColor)),
      home: SplashScreen(),
      debugShowCheckedModeBanner: false,
      routes: <String, WidgetBuilder>{
        "/a": (BuildContext context) => Homepage(),
      },
    );
  }
}