import 'package:http/http.dart';
import 'dart:convert';

import 'package:rightware_app/models/shop.dart';

class API {
  final String _uri =
      'https://api.orianosy.com/shop/test/find/all/shop?deviceKind=android';
  final String _secretKey = '2a2xExPa!&+bO9KS!aMC';

  Future<List<Shop>> getShops() async {
    List<Shop> shops = [];
    try {
      Response response =
          await get(Uri.parse(_uri), headers: {'secretKey': _secretKey});
      if (response.statusCode == 200) {
        String data = response.body;
        List<dynamic> jsonData = jsonDecode(data)['result'];
        for (int i = 0; i < jsonData.length; i++) {
          shops.add(Shop.fromJson(jsonData[i]));
        }
        return shops;
      } else {
        print('status code = ${response.statusCode}');
      }
    } catch (e) {
      print(e.toString());
    }
    return shops;
  }

  @override
  String toString() {
    return 'API{secretKey: $_secretKey, uri: $_uri}';
  }
}
