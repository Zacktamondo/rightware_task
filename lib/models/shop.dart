import 'package:rightware_app/models/address.dart';
import 'package:rightware_app/models/minimium_order.dart';
import 'package:rightware_app/models/multilang.dart';

class Shop {
  Multilang? shopName, description;
  Address? address;
  List<dynamic>? deliveryRegions;
  String? coverPhoto, estimatedDeliveryTime;
  MinimumOrder? minimumOrder;

  Shop.fromJson(Map<String, dynamic> json) {
    shopName = Multilang.fromMap(json['shopName']);
    description = Multilang.fromMap(json['description']);
    address = Address.fromJson(json['address']);
    deliveryRegions = json['deliveryRegions'];
    coverPhoto = json['coverPhoto'];
    estimatedDeliveryTime = json['estimatedDeliveryTime'];
    minimumOrder = MinimumOrder.fromMap(json['minimumOrder']);
  }

  @override
  String toString() {
    return 'Shop{shopName: $shopName, description: $description, address: $address, deliveryRegions: $deliveryRegions, coverPhoto: $coverPhoto, estimatedDeliveryTime: $estimatedDeliveryTime, minimumOrder: $minimumOrder}';
  }
}
