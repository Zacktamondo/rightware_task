class Multilang{
   final String en , ar;

  Multilang({required this.en, required this.ar});

factory Multilang.fromMap(Map<String,dynamic> map){
    return Multilang(
      en: map['en'],
      ar: map['ar'],
    );
  }

   @override
  String toString() {
    return 'TranslatedString{en: $en, ar: $ar}';
  }
}